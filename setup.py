from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='TCC do MBA em CiÃªncia de Dados do ICMC-USP',
    author='Matheus Mendes dos Santos',
    license='MIT',
)
